sumaimpares = 0
seguir = True
numero1 = int(input("dame un numero entero no negativo: "))
while numero1 < 0:
    numero1 = int(input("dame un numero entero NO negativo: "))

numero2 = int(input("dame otro numero entero no negativo: "))
while numero2 < 0:
    numero2 = int(input("dame un numero entero NO negativo: "))

n1 = min(numero1, numero2)
n2 = max(numero1, numero2)

#si no queremos que los numeros dados se incluyan en la suma, solo hace falta poner el for i in range(n1, n2)
if n1 % 2 != 0:
    sumaimpares += n1
else:
    seguir
for i in range(n1 + 1, n2):
    if i % 2 != 0:
        sumaimpares += i
    else:
        seguir
if n2 % 2 != 0:
        sumaimpares += n2
else:
        seguir
print("la suma de los impares que están comprendidos entre", n1, "y", n2, "es: ", sumaimpares)